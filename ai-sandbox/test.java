/* Given a string s, return the longest 
palindromic substring in s.

Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"
 
Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters.

*/

class Solution
{
    public String longestPalindrome(String s)
    {
        String longest = "";
        
        for(int i = 0; i < s.length(); i++)
        {
            String odd = expand(s, i, i);
            String even = expand(s, i, i+1);
            
            if(odd.length() > longest.length())
            {
                longest = odd;
            }
            
            if(even.length() > longest.length())
            {
                longest = even;
            }
        }

        return longest;
    }
    
    public String expand(String s, int left, int right)
    {
        while(left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right))
        {
            left--;
            right++;
        }

        return s.substring(left+1, right);
    }
}
