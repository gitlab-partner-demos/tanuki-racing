import pandas as pd  # programmers like shortening names for things, so they usually import pandas as "pd"
import altair as alt # again with the shortening of names
import requests # we use it for downloading the data so we don't have to save it on GitHub (too big!)
import zipfile # for de-compressing the files we download from the EPA

# Download the data from the EPA website
data_file_urls = [
    'https://aqs.epa.gov/aqsweb/airdata/daily_88101_2020.zip',
    'https://aqs.epa.gov/aqsweb/airdata/daily_88101_2019.zip'
]
# copied this example from https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests
for url in data_file_urls:
    local_filename = "data/{}".format(url.split('/')[-1])
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                f.write(chunk)
# and unzip the files
files_to_unzip = ["data/{}".format(url.split('/')[-1]) for url in data_file_urls]
for f in files_to_unzip:
    with zipfile.ZipFile(f,"r") as zip_ref:
        zip_ref.extractall("data")

air_2019_df = pd.read_csv("data/daily_88101_2019.csv")
air_2020_df = pd.read_csv("data/daily_88101_2020.csv")
air_2020_df.head() # this helpfully prints out the first few rows with headers to preview the data

"""
Aggregate and average MA data by city 
"""



